Vielen Dank für Ihre E-Mail! Für diesen wurde eine Vorgangsnummer %{ISSUE_ID} vergeben. Wir werden diesen Vorgang zügig kommentieren. Sie können ihn im Projekt-Board finden: https://gitlab.opencode.de/fitko/fim/fim-portal-relaunch-forum/-/boards.

Ihr Vorgang ist solange nicht sichtbar, wie er von uns nicht bearbeitet und veröffentlicht wurde. 

Ihre möglicherweise in der Signatur Ihrer E-Mail hinterlegten Kontaktdaten werden wie alle anderen Inhalte Ihrer E-Mail bei Veröffentlichung ebenfalls öffentlich sichtbar sein. Sollten Sie das nicht wünschen, dann antworten Sie bitte zügig auf diese E-Mail und fordern Löschung Ihrer Kontaktdaten im Vorgang.