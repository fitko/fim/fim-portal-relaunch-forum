## Warum machen wir das?
<!-- Eine kurze Erklärung des Warum, nicht des Was oder Wie. Gehen Sie davon aus, dass der/die Leser:in die Hintergründe nicht kennt und keine Zeit hat, Informationen aus Kommentaren herauszusuchen. -->

Als ein Ladenbesitzer,

möchte ich, dass die Produkte und Preise der Geschäfte in der Umgebung im Informationsfenster des Ladens angezeigt werden,

sodass die potenziellen Kunden mehr über das Geschäft und die verkauften Produkte erfahren und eine Kaufentscheidung treffen können.

## Relevante Informationen
<!-- Informationen, die im Kontext der Umsetzung dieser User Story relevant sind. -->

- Wichtig ist, dass...

## Akzeptanzkriterien
<!-- Was muss erfüllt sein, damit diese User Story als erledigt angesehen werden kann. -->

1. [ ] ...
2. [ ] ...
3. [ ] ...

